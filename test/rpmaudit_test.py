#!/usr/bin/python
#
# Author: Maer Melo
#         salalbr [at] gmail [dot] com
#
# test cases for rpmaudit

import unittest, sys, os, subprocess, random
sys.path.append('../')
import rpmaudit

def InstallTestRpm():
    query = ['rpm', '-i', 'site-config-0.2-3.1.1.x86_64.rpm' ]
    p = subprocess.Popen(query, stdout=subprocess.PIPE)
    (output, err) = p.communicate()

def UninstallTestRpm():
    query = ['rpm', '-e', 'site-config']
    p = subprocess.Popen(query, stdout=subprocess.PIPE)
    (output, err) = p.communicate()

class TestInstallTestRpm(unittest.TestCase):
    def setUp(self):
        self.auditing = rpmaudit.Rpmaudit()
        InstallTestRpm()
        self.check = self.auditing.generate_delta()
    def tearDown(self):
        UninstallTestRpm()
    def test_install(self):
        self.assertTrue(any('site-config' in s for s in self.check))

if __name__ == '__main__':
    unittest.main()

