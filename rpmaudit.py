#!/usr/bin/python
#
# Author: Maer Melo
#         salalbr [at] gmail [dot] com
#
# rpmaudit - Nearly real-time rpm changes logging

import rpm, sys, hashlib, json, time

class Rpmaudit:
    def __init__(self):
        self.prev_snapshot = self.build_snapshot()
        self.prev_md5 = self.calculate_md5(self.prev_snapshot)
        
    # Method to get name, version, release, and installtime of all packages
    def build_snapshot(self):
        rpm_ts = rpm.TransactionSet()
        rpm_it = rpm_ts.dbMatch()
        packages = {}
        for doc in rpm_it:
            packages[doc['name']] = { 'version': doc['version'],\
            'release': doc['release'], 'installtime': doc['installtime'] }
        return packages

    # Hash method to calculate rpm query MD5
    def calculate_md5(self, dict):
        dict_md5 = hashlib.md5(json.dumps(dict, sort_keys=True)).hexdigest()
        return str(dict_md5)

    def generate_delta(self):
        cur_snapshot = self.build_snapshot()
        cur_md5 = self.calculate_md5(cur_snapshot)
        rows = list()
        if self.prev_md5 != cur_md5:
            prev_rpm_list, cur_rpm_list = (self.prev_snapshot.keys(),\
                cur_snapshot.keys())
            rpm_dif = list(set(prev_rpm_list).\
                symmetric_difference(set(cur_rpm_list)))
            rpm_int = list(set(prev_rpm_list).\
                intersection(set(cur_rpm_list)))
            for package in rpm_dif:
                if package in self.prev_snapshot.keys():
                    rows.append(('Uninstall', time.time(), package,\
                        self.prev_snapshot[package]['version'],\
                            self.prev_snapshot[package]['release']))
                else:
                    rows.append(('Install', cur_snapshot[package]\
                        ['installtime'], package, cur_snapshot[package]\
                        ['version'], cur_snapshot[package]['release']))
            for package in rpm_int:
                if self.prev_snapshot[package]['version'] !=\
                    cur_snapshot[package]['version'] \
                    or self.prev_snapshot[package]['release'] !=\
                    cur_snapshot[package]['release'] \
                    or self.prev_snapshot[package]['installtime'] !=\
                    cur_snapshot[package]['installtime']:
                        rows.append(('Upgrade', cur_snapshot[package]\
                            ['installtime'], package,\
                        cur_snapshot[package]['version'],\
                            cur_snapshot[package]['release']))
            self.prev_snapshot, self.prev_md5 = (cur_snapshot, cur_md5)
            return rows

# Logging function - print out events to /var/log/rpmaudit
def logging(events, out_file='/var/log/rpmaudit'):
    install_msg = ' has been installed'
    uninstall_msg = ' has been uninstalled'
    upgrade_msg = ' has been upgraded/downgraded'
    f = open(out_file, 'a+b')
    for i in range(len(events)):
        if events[i][0] == 'Install':
            f.write('[ ' + str(time.strftime("%D %H:%M:%S",\
            time.localtime(int(events[i][1])))) + ' ]  [ ' + events[i][0] +\
            ' ]  [ Package ' + events[i][2] + '-' + events[i][3] +\
            '-' + events[i][4] + install_msg + ' ]\n')
        elif events[i][0] == 'Uninstall':
            f.write('[ ' + str(time.strftime("%D %H:%M:%S",\
            time.localtime(int(events[i][1])))) + ' ]  [ ' + events[i][0] +\
            ' ]  [ Package ' + events[i][2] + '-' + events[i][3] +\
            '-' + events[i][4] + uninstall_msg + ' ]\n')
        else:
            f.write('[ ' + str(time.strftime("%D %H:%M:%S",\
            time.localtime(int(events[i][1])))) + ' ]  [ ' + events[i][0] +\
            ' ]  [ Package ' + events[i][2] + '-' + events[i][3] +\
            '-' + events[i][4] + upgrade_msg + ' ]\n')
    f.close()

if __name__ == "__name__":
    auditing = Rpmaudit()
    while(True):
        time.sleep(5)
        delta = auditing.generate_delta()
        if delta != None:
            logging(delta)

